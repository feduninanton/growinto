package ru.fedunin.growinto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrowintoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrowintoApplication.class, args);
    }

}
