package ru.fedunin.growinto.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.fedunin.growinto.dto.GrowthClothesForm;
import ru.fedunin.growinto.service.GrowthClothesService;

@Controller
@RequiredArgsConstructor
public class GrowthClothesController {

    private final GrowthClothesService growthClothesService;

    @PostMapping("/growthclothes")
    public String addGrowthClothes(GrowthClothesForm growthClothes) {
        growthClothesService.addGrowthClothes(growthClothes);
    return "redirect:/growthclothes";
    }

    @GetMapping("/growthclothes")
    public String getGrowthClothes( @RequestParam(value = "orderBy", required = false) String orderBy,
                               @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("GrowthClothes", growthClothesService.getAllGrowthClothes());
        return "growthclothes_page";
    }

    @GetMapping("/growthclothes/{growthClothes-id}")
    public String getGrowthClothesPage(@PathVariable("growthClothes-id")long growthClothesId, Model model) {
        model.addAttribute("GrowthClothes", growthClothesService.getGrowthClothes(growthClothesId));
        return "itemClothes_page";
    }

    @GetMapping("/growthclothes/{growthClothes-id}/delete")
    public String deleteGrowthClothes(@PathVariable("growthClothes-id") Long growthClothesId){
        growthClothesService.deleteGrowthClothes(growthClothesId);
        return "redirect:/growthclothes";
    }

    @PostMapping("/growthclothes/{growthClothes-id}/update")
    public String updateGrowthClothes(@PathVariable("growthClothes-id") Long growthClothesId, GrowthClothesForm growthClothes) {
        growthClothesService.updateGrowthClothes(growthClothesId, growthClothes);
        return "redirect:/growthclothes";
    }



}
