package ru.fedunin.growinto.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.fedunin.growinto.dto.GrowthClothesForm;
import ru.fedunin.growinto.models.GrowthClothes;
import ru.fedunin.growinto.repository.GrowthClothesRepository;
import ru.fedunin.growinto.service.GrowthClothesService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GrowthClothesServiceImpl implements GrowthClothesService {

    private  final GrowthClothesRepository growthClothesRepository;

    @Override
    public void deleteGrowthClothes(Long growthClothesId) {
        GrowthClothes clothesForDelete = growthClothesRepository.findById(growthClothesId).orElseThrow();
        growthClothesRepository.delete(clothesForDelete);
    }

    @Override
    public List<GrowthClothes> getAllGrowthClothes() {
        return growthClothesRepository.findAll();
    }

    @Override
    public void addGrowthClothes(GrowthClothesForm growthClothes) {
    GrowthClothes newGrowthClothes = GrowthClothes.builder()
            .title(growthClothes.getTitle())
            .description(growthClothes.getDescription())
            .build();
        growthClothesRepository.save(newGrowthClothes);
    }

    @Override
    public GrowthClothes getGrowthClothes(long growthClothesId) {
        return growthClothesRepository.findById(growthClothesId).orElseThrow();
    }

    @Override
    public void updateGrowthClothes(Long growthClothesId, GrowthClothesForm updateData) {
        GrowthClothes clothesForUpdate = growthClothesRepository.findById(growthClothesId).orElseThrow();
        clothesForUpdate.setTitle(updateData.getTitle());
        clothesForUpdate.setDescription(updateData.getDescription());
        growthClothesRepository.save(clothesForUpdate);
    }
}
