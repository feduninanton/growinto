package ru.fedunin.growinto.service;

import java.util.List;
import ru.fedunin.growinto.dto.GrowthClothesForm;
import ru.fedunin.growinto.models.GrowthClothes;

public interface GrowthClothesService {

    void deleteGrowthClothes(Long growthClothesId);

    List<GrowthClothes> getAllGrowthClothes();

    void addGrowthClothes(GrowthClothesForm growthClothes);

    GrowthClothes getGrowthClothes(long growthClothesId);

    void updateGrowthClothes(Long growthClothesId, GrowthClothesForm growthClothes);
}
