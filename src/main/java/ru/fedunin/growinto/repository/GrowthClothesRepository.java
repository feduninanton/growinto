package ru.fedunin.growinto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.fedunin.growinto.models.GrowthClothes;

public interface GrowthClothesRepository extends JpaRepository <GrowthClothes,Long> {
}
